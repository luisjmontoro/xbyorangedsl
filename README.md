# Basic Style Dictionary

This example code is bare-bones to show you what this framework can do. If you have the style-dictionary module installed globally, just cd into this directory and run:
```bash
style-dictionary build
```

You should see something like this output:
```
Reading config file from ./config.json
Building all platforms

scss
✔︎ build/scss/_variables.scss

android
✔︎ build/android/font_dimens.xml

ios
✔︎ build/ios/StyleDictionaryColor.h
✔︎ build/ios/StyleDictionaryColor.m
```

