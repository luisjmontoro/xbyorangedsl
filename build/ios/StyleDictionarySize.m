
//
// StyleDictionarySize.m
//
// Do not edit directly
// Generated on Tue, 19 Feb 2019 09:28:41 GMT
//

#import "StyleDictionarySize.h"



float const SizeSpacingComponentXxxs = 2.00f;
float const SizeSpacingComponentXxs = 4.00f;
float const SizeSpacingComponentXs = 8.00f;
float const SizeSpacingComponentS = 12.00f;
float const SizeSpacingComponentM = 16.00f;
float const SizeSpacingComponentL = 24.00f;
float const SizeSpacingComponentXl = 32.00f;
float const SizeSpacingComponentXxl = 40.00f;
float const SizeSpacingComponentXxxl = 48.00f;
float const SizeSpacingLayoutXxs = 16.00f;
float const SizeSpacingLayoutXs = 24.00f;
float const SizeSpacingLayoutS = 32.00f;
float const SizeSpacingLayoutM = 48.00f;
float const SizeSpacingLayoutL = 80.00f;
float const SizeSpacingLayoutXl = 96.00f;
float const SizeSpacingLayoutXxl = 160.00f;
float const SizeFontDesktopXxs = 10.00f;
float const SizeFontDesktopXs = 12.00f;
float const SizeFontDesktopS = 14.00f;
float const SizeFontDesktopM = 16.00f;
float const SizeFontDesktopL = 20.00f;
float const SizeFontDesktopXl = 24.00f;
float const SizeFontDesktopXxl = 28.00f;
float const SizeFontDesktopHead3 = 32.00f;
float const SizeFontDesktopHead2 = 46.00f;
float const SizeFontDesktopHead1 = 58.00f;
float const SizeFontDesktopDisplay3 = 64.00f;
float const SizeFontDesktopDisplay2 = 72.00f;
float const SizeFontDesktopDisplay1 = 80.00f;
float const SizeFontTabletXxs = 10.00f;
float const SizeFontTabletXs = 12.00f;
float const SizeFontTabletS = 14.00f;
float const SizeFontTabletM = 16.00f;
float const SizeFontTabletL = 18.00f;
float const SizeFontTabletXl = 20.00f;
float const SizeFontTabletXxl = 24.00f;
float const SizeFontTabletHead3 = 28.00f;
float const SizeFontTabletHead2 = 32.00f;
float const SizeFontTabletHead1 = 36.00f;
float const SizeFontTabletDisplay3 = 52.00f;
float const SizeFontTabletDisplay2 = 58.00f;
float const SizeFontTabletDisplay1 = 64.00f;
float const SizeFontMobileXxs = 12.00f;
float const SizeFontMobileXs = 12.00f;
float const SizeFontMobileS = 14.00f;
float const SizeFontMobileM = 16.00f;
float const SizeFontMobileL = 18.00f;
float const SizeFontMobileXl = 20.00f;
float const SizeFontMobileXxl = 24.00f;
float const SizeFontMobileHead3 = 28.00f;
float const SizeFontMobileHead2 = 32.00f;
float const SizeFontMobileHead1 = 36.00f;
float const SizeFontMobileDisplay3 = 46.00f;
float const SizeFontMobileDisplay2 = 52.00f;
float const SizeFontMobileDisplay1 = 58.00f;
float const SizeFontSmall = 12.00f;
float const SizeFontMedium = 16.00f;
float const SizeFontLarge = 32.00f;
float const SizeFontBase = 16.00f;
