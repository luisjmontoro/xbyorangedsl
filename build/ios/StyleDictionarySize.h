
// StyleDictionarySize.h
//
// Do not edit directly
// Generated on Tue, 19 Feb 2019 09:28:41 GMT
//

#import <Foundation/Foundation.h>



extern float const SizeSpacingComponentXxxs;
extern float const SizeSpacingComponentXxs;
extern float const SizeSpacingComponentXs;
extern float const SizeSpacingComponentS;
extern float const SizeSpacingComponentM;
extern float const SizeSpacingComponentL;
extern float const SizeSpacingComponentXl;
extern float const SizeSpacingComponentXxl;
extern float const SizeSpacingComponentXxxl;
extern float const SizeSpacingLayoutXxs;
extern float const SizeSpacingLayoutXs;
extern float const SizeSpacingLayoutS;
extern float const SizeSpacingLayoutM;
extern float const SizeSpacingLayoutL;
extern float const SizeSpacingLayoutXl;
extern float const SizeSpacingLayoutXxl;
extern float const SizeFontDesktopXxs;
extern float const SizeFontDesktopXs;
extern float const SizeFontDesktopS;
extern float const SizeFontDesktopM;
extern float const SizeFontDesktopL;
extern float const SizeFontDesktopXl;
extern float const SizeFontDesktopXxl;
extern float const SizeFontDesktopHead3;
extern float const SizeFontDesktopHead2;
extern float const SizeFontDesktopHead1;
extern float const SizeFontDesktopDisplay3;
extern float const SizeFontDesktopDisplay2;
extern float const SizeFontDesktopDisplay1;
extern float const SizeFontTabletXxs;
extern float const SizeFontTabletXs;
extern float const SizeFontTabletS;
extern float const SizeFontTabletM;
extern float const SizeFontTabletL;
extern float const SizeFontTabletXl;
extern float const SizeFontTabletXxl;
extern float const SizeFontTabletHead3;
extern float const SizeFontTabletHead2;
extern float const SizeFontTabletHead1;
extern float const SizeFontTabletDisplay3;
extern float const SizeFontTabletDisplay2;
extern float const SizeFontTabletDisplay1;
extern float const SizeFontMobileXxs;
extern float const SizeFontMobileXs;
extern float const SizeFontMobileS;
extern float const SizeFontMobileM;
extern float const SizeFontMobileL;
extern float const SizeFontMobileXl;
extern float const SizeFontMobileXxl;
extern float const SizeFontMobileHead3;
extern float const SizeFontMobileHead2;
extern float const SizeFontMobileHead1;
extern float const SizeFontMobileDisplay3;
extern float const SizeFontMobileDisplay2;
extern float const SizeFontMobileDisplay1;
extern float const SizeFontSmall;
extern float const SizeFontMedium;
extern float const SizeFontLarge;
extern float const SizeFontBase;
