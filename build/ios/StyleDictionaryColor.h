
//
// StyleDictionaryColor.h
//
// Do not edit directly
// Generated on Tue, 19 Feb 2019 09:28:41 GMT
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, StyleDictionaryColorName) {
ColorFontBlack,
ColorFontNeutral,
ColorFontIndigoBase,
ColorFontIndigoBase75,
ColorFontIndigoBase50,
ColorFontWhite,
ColorFontInfo,
ColorFontSuccess,
ColorFontWarning,
ColorFontError,
ColorFontLink,
ColorFontPrimaryBase,
ColorFontPrimaryBase50,
ColorFontFamily1,
ColorFontFamily2,
ColorFontFamily3,
ColorFontFamily4,
ColorFontGlobalBase,
ColorRootBrandPrimaryBase,
ColorRootBrandPrimaryBase75,
ColorRootBrandPrimaryBase50,
ColorRootBrandPrimaryBase25,
ColorRootBrandFamily1Base,
ColorRootBrandFamily1Base75,
ColorRootBrandFamily1Base50,
ColorRootBrandFamily1Base25,
ColorRootBrandFamily2Base,
ColorRootBrandFamily2Base75,
ColorRootBrandFamily2Base50,
ColorRootBrandFamily2Base25,
ColorRootBrandFamily3Base,
ColorRootBrandFamily3Base75,
ColorRootBrandFamily3Base50,
ColorRootBrandFamily3Base25,
ColorRootBrandFamily4Base,
ColorRootBrandFamily4Base75,
ColorRootBrandFamily4Base50,
ColorRootBrandFamily4Base25,
ColorRootBrandGlobalBase,
ColorRootBrandGlobalDark,
ColorRootBrandGlobalLight,
ColorRootUiNeutralBase,
ColorRootUiNeutralBase90,
ColorRootUiNeutralBase80,
ColorRootUiNeutralBase70,
ColorRootUiNeutralBase60,
ColorRootUiNeutralBase50,
ColorRootUiNeutralBase40,
ColorRootUiNeutralBase30,
ColorRootUiNeutralBase20,
ColorRootUiNeutralBase10,
ColorRootUiNeutralBlack,
ColorRootUiNeutralWhite,
ColorRootUiSupportSuccessBase,
ColorRootUiSupportSuccessBase75,
ColorRootUiSupportSuccessBase50,
ColorRootUiSupportSuccessBase25,
ColorRootUiSupportSuccessText,
ColorRootUiSupportErrorBase,
ColorRootUiSupportErrorBase75,
ColorRootUiSupportErrorBase50,
ColorRootUiSupportErrorBase25,
ColorRootUiSupportErrorText,
ColorRootUiSupportWarningBase,
ColorRootUiSupportWarningBase75,
ColorRootUiSupportWarningBase50,
ColorRootUiSupportWarningBase25,
ColorRootUiSupportWarningText,
ColorRootUiSupportInfoBase,
ColorRootUiSupportInfoBase75,
ColorRootUiSupportInfoBase50,
ColorRootUiSupportInfoBase25,
ColorRootUiSupportInfoText,
ColorRootUiTextBase,
ColorRootUiTextBase75,
ColorRootUiTextBase50,
ColorRootUiTextBase25,
ColorRootUiLinkDefault,
ColorRootUiLinkHover,
ColorRootUiLinkActive,
ColorRootUiLinkVisited,
ColorRootUiOpacityTransparent,
ColorRootUiSocialBehance,
ColorRootUiSocialDribbble,
ColorRootUiSocialDropbox,
ColorRootUiSocialFacebook,
ColorRootUiSocialGithub,
ColorRootUiSocialGooglePlus,
ColorRootUiSocialInstagram,
ColorRootUiSocialLinkedin,
ColorRootUiSocialPinterest,
ColorRootUiSocialSkype,
ColorRootUiSocialSlack,
ColorRootUiSocialSpotify,
ColorRootUiSocialTwitter,
ColorRootUiSocialVimeo,
ColorRootUiSocialYoutube
};

@interface StyleDictionaryColor : NSObject
+ (NSArray *)values;
+ (UIColor *)color:(StyleDictionaryColorName)color;
@end
