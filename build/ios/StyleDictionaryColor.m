
//
// StyleDictionaryColor.m
//
// Do not edit directly
// Generated on Tue, 19 Feb 2019 09:28:41 GMT
//

#import "StyleDictionaryColor.h"


@implementation StyleDictionaryColor

+ (UIColor *)color:(StyleDictionaryColorName)colorEnum{
  return [[self values] objectAtIndex:colorEnum];
}

+ (NSArray *)values {
  static NSArray* colorArray;
  static dispatch_once_t onceToken;

  dispatch_once(&onceToken, ^{
    colorArray = @[
[UIColor colorWithRed:0.00f green:0.00f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:0.13f green:0.13f blue:0.13f alpha:1.00f],
[UIColor colorWithRed:0.18f green:0.20f blue:0.26f alpha:1.00f],
[UIColor colorWithRed:0.39f green:0.40f blue:0.45f alpha:1.00f],
[UIColor colorWithRed:0.59f green:0.60f blue:0.63f alpha:1.00f],
[UIColor colorWithRed:1.00f green:1.00f blue:1.00f alpha:1.00f],
[UIColor colorWithRed:0.08f green:0.36f blue:0.95f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.63f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:0.75f green:0.60f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:0.70f green:0.15f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:0.31f green:0.15f blue:0.65f alpha:1.00f],
[UIColor colorWithRed:0.31f green:0.15f blue:0.65f alpha:1.00f],
[UIColor colorWithRed:0.65f green:0.57f blue:0.82f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.75f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.37f blue:0.37f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.81f blue:0.79f alpha:1.00f],
[UIColor colorWithRed:0.03f green:0.85f blue:0.47f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.47f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:0.31f green:0.15f blue:0.65f alpha:1.00f],
[UIColor colorWithRed:0.48f green:0.36f blue:0.74f alpha:1.00f],
[UIColor colorWithRed:0.65f green:0.57f blue:0.82f alpha:1.00f],
[UIColor colorWithRed:0.83f green:0.79f blue:0.91f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.75f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.81f blue:0.25f alpha:1.00f],
[UIColor colorWithRed:0.99f green:0.87f blue:0.49f alpha:1.00f],
[UIColor colorWithRed:0.98f green:0.92f blue:0.73f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.37f blue:0.37f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.53f blue:0.53f alpha:1.00f],
[UIColor colorWithRed:0.99f green:0.68f blue:0.68f alpha:1.00f],
[UIColor colorWithRed:0.98f green:0.83f blue:0.83f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.81f blue:0.79f alpha:1.00f],
[UIColor colorWithRed:0.25f green:0.85f blue:0.84f alpha:1.00f],
[UIColor colorWithRed:0.49f green:0.89f blue:0.89f alpha:1.00f],
[UIColor colorWithRed:0.73f green:0.94f blue:0.93f alpha:1.00f],
[UIColor colorWithRed:0.03f green:0.85f blue:0.47f alpha:1.00f],
[UIColor colorWithRed:0.27f green:0.89f blue:0.60f alpha:1.00f],
[UIColor colorWithRed:0.51f green:0.91f blue:0.73f alpha:1.00f],
[UIColor colorWithRed:0.74f green:0.95f blue:0.85f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.47f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.00f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:1.00f green:1.00f blue:1.00f alpha:1.00f],
[UIColor colorWithRed:0.13f green:0.13f blue:0.13f alpha:1.00f],
[UIColor colorWithRed:0.22f green:0.22f blue:0.22f alpha:1.00f],
[UIColor colorWithRed:0.31f green:0.31f blue:0.31f alpha:1.00f],
[UIColor colorWithRed:0.39f green:0.39f blue:0.39f alpha:1.00f],
[UIColor colorWithRed:0.48f green:0.48f blue:0.48f alpha:1.00f],
[UIColor colorWithRed:0.56f green:0.56f blue:0.56f alpha:1.00f],
[UIColor colorWithRed:0.65f green:0.65f blue:0.65f alpha:1.00f],
[UIColor colorWithRed:0.74f green:0.74f blue:0.74f alpha:1.00f],
[UIColor colorWithRed:0.82f green:0.82f blue:0.82f alpha:1.00f],
[UIColor colorWithRed:0.91f green:0.91f blue:0.91f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.00f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:1.00f green:1.00f blue:1.00f alpha:1.00f],
[UIColor colorWithRed:0.20f green:0.78f blue:0.20f alpha:1.00f],
[UIColor colorWithRed:0.40f green:0.84f blue:0.40f alpha:1.00f],
[UIColor colorWithRed:0.60f green:0.89f blue:0.60f alpha:1.00f],
[UIColor colorWithRed:0.80f green:0.95f blue:0.80f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.63f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:0.86f green:0.29f blue:0.14f alpha:1.00f],
[UIColor colorWithRed:0.89f green:0.47f blue:0.35f alpha:1.00f],
[UIColor colorWithRed:0.93f green:0.65f blue:0.57f alpha:1.00f],
[UIColor colorWithRed:0.96f green:0.82f blue:0.78f alpha:1.00f],
[UIColor colorWithRed:0.70f green:0.15f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.80f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.85f blue:0.25f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.90f blue:0.50f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.95f blue:0.75f alpha:1.00f],
[UIColor colorWithRed:0.75f green:0.60f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:0.27f green:0.49f blue:0.95f alpha:1.00f],
[UIColor colorWithRed:0.45f green:0.62f blue:0.96f alpha:1.00f],
[UIColor colorWithRed:0.64f green:0.75f blue:0.98f alpha:1.00f],
[UIColor colorWithRed:0.82f green:0.87f blue:0.98f alpha:1.00f],
[UIColor colorWithRed:0.08f green:0.36f blue:0.95f alpha:1.00f],
[UIColor colorWithRed:0.18f green:0.20f blue:0.26f alpha:1.00f],
[UIColor colorWithRed:0.39f green:0.40f blue:0.45f alpha:1.00f],
[UIColor colorWithRed:0.59f green:0.60f blue:0.63f alpha:1.00f],
[UIColor colorWithRed:0.79f green:0.80f blue:0.81f alpha:1.00f],
[UIColor colorWithRed:0.31f green:0.15f blue:0.65f alpha:1.00f],
[UIColor colorWithRed:0.65f green:0.57f blue:0.82f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.00f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.00f blue:0.00f alpha:1.00f],
[UIColor colorWithRed:1.00f green:1.00f blue:1.00f alpha:0.00f],
[UIColor colorWithRed:0.09f green:0.41f blue:1.00f alpha:1.00f],
[UIColor colorWithRed:0.92f green:0.30f blue:0.54f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.38f blue:1.00f alpha:1.00f],
[UIColor colorWithRed:0.23f green:0.35f blue:0.60f alpha:1.00f],
[UIColor colorWithRed:0.25f green:0.47f blue:0.75f alpha:1.00f],
[UIColor colorWithRed:0.83f green:0.28f blue:0.21f alpha:1.00f],
[UIColor colorWithRed:0.30f green:0.37f blue:0.84f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.47f blue:0.71f alpha:1.00f],
[UIColor colorWithRed:0.80f green:0.13f blue:0.15f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.69f blue:0.94f alpha:1.00f],
[UIColor colorWithRed:0.43f green:0.79f blue:0.86f alpha:1.00f],
[UIColor colorWithRed:0.12f green:0.84f blue:0.38f alpha:1.00f],
[UIColor colorWithRed:0.00f green:0.71f blue:0.95f alpha:1.00f],
[UIColor colorWithRed:0.27f green:0.73f blue:1.00f alpha:1.00f],
[UIColor colorWithRed:1.00f green:0.00f blue:0.00f alpha:1.00f]
    ];
  });

  return colorArray;
}

@end
